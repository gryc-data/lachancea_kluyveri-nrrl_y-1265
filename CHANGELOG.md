# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.0 (2021-05-05)

### Added

* The 7 annotated chromosomes of Lachancea kluyveri NRRL Y-1265 (source EBI, [GCA_000149225.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000149225.1)).
