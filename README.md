## *Lachancea kluyveri* NRLL Y-1265

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJNA1445](https://www.ebi.ac.uk/ena/browser/view/PRJNA1445)
* **Assembly accession**: [GCA_000149225.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000149225.1)
* **Original submitter**: Washington University School of Medicine

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM14922v1
* **Assembly length**: 11,509,127
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 1,295,560 (4)

### Annotation overview

**No public annotation available**
